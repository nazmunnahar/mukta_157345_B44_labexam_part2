<html>
<head>
    <title>Chessboard with PHP, HTML and CSS </title>
    <style type="text/css">
        div {
            float: left;
            width: 50px;
            height: 50px;
            border-style: solid;
            border-width: 1px;
        }

        .black {
            background: black;
        }
        .white {
            background: white;
        }

    </style>

</head>

<?php

echo "<form action='' method='post'>
       <input type='text' name='num' placeholder='Enter a number'>
       <input type='submit' >
       </form>" ;

 $num = $_POST['num'];


for ($row=0; $row < $num; $row++) {

    for($col=0; $col < $num; $col++){

        if($col%$num==0)
            echo "<br style=\"clear:both\" />";
        if($col%2==0){
            if($row%2==0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }
        else{
            if($row%2!=0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }

    }

}

?>